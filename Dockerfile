FROM maven:latest

COPY src /home/datasource/src
COPY pom.xml /home/datasource

WORKDIR /home/datasource

RUN mvn clean package 
CMD java -jar target/*.jar