package com.openset.datasource.twitter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.openset.datasource.types.Datasource;
import com.openset.datasource.types.Twitter;

@SpringBootTest
@TestPropertySource(locations = "classpath:twitter-test.properties",
					properties = { "datasource.type=twitter", "datasource.topics=informatica" })
@EnabledIfEnvironmentVariable(named = "TWITTER_APP_KEY", matches = ".+")
public class TwitterTest {

	@Autowired
	private Datasource datasource;

	@Test
	public void whenDatasourceTypeIsTwitter_thenDatasourceShouldBeInstanceOfTwitter() {
		assertTrue(datasource instanceof Twitter);
	}

	@Test
	public void testTweets() {
		assertNotNull(datasource.queryData("sicurezza", "informatica"));
	}

}
