package com.openset.datasource.xml;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.openset.datasource.types.Datasource;
import com.openset.datasource.types.XML;

@SpringBootTest
@TestPropertySource(locations = { "classpath:unit-test.properties" },
					properties = {  "datasource.type=xml",
									"datasource.uri=https://openset.altervista.org/lombardia_impianti.xml",
									"datasource.topics=lombardia"})
public class RemoteXMLTest {

	@Autowired
	private Datasource datasource;
	
	@Test
	public void whenDatasourceTypeIsXML_thenDatasourceShouldBeInstanceOfXML() {
		assertTrue(datasource instanceof XML);
	}
	
	@Test
	public void testSearch1() {
		assertFalse(datasource.queryData("segreteria", "lombardia").isEmpty());
	}
	
	@Test
	public void testSearch2() {
		assertTrue(datasource.queryData("segreteria", "lombardia").length() > 20);
	}
	
	@Test
	public void testSearch3() {
		assertFalse(datasource.queryData("delibera", "Lombardia").isEmpty());
	}
	
	@Test
	public void testEmptySearch() {
		assertTrue(datasource.queryData("fdfgvsdfdsa", "Lombardia").isEmpty());
	}
}
