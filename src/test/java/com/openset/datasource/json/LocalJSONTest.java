package com.openset.datasource.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.openset.datasource.types.Datasource;
import com.openset.datasource.types.JSON;
import com.openset.datasource.types.json.JsonArray;

@SpringBootTest
@TestPropertySource(locations = { "classpath:unit-test.properties" },
					properties = {  "datasource.type=json",
									"datasource.uri=src/test/resources/local/origin.json",
									"datasource.topics=games"})
public class LocalJSONTest {

	@Autowired
	private Datasource datasource;
	
	@Test
	public void whenDatasourceTypeIsJson_ThenDatasourceShouldBeInstanceOfJSON() {
		assertTrue(datasource instanceof JSON);		
	}
	
	@Test
	public void testSearch1() {
		JsonArray result = datasource.queryData("dragon age inquisition", "games");
		assertEquals(8, result.length());
	}
}
