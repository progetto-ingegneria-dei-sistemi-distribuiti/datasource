package com.openset.datasource.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.openset.datasource.exceptions.JsonParsingException;
import com.openset.datasource.types.json.Json;
import com.openset.datasource.types.json.JsonArray;
import com.openset.datasource.types.json.JsonObject;

@SpringBootTest
@TestPropertySource(locations = "classpath:unit-test.properties")
public class JsonTest {

	@Test
	public void testValidJson_withAJsonObject() {
		String jsonObject = "{\"name\":\"John\", \"age\":31, \"city\":\"New York\"}";
		String jsonObject2 = "{\n" + 
				"    \"glossary\": {\n" + 
				"        \"title\": \"example glossary\",\n" + 
				"		\"GlossDiv\": {\n" + 
				"            \"title\": \"S\",\n" + 
				"			\"GlossList\": {\n" + 
				"                \"GlossEntry\": {\n" + 
				"                    \"ID\": \"SGML\",\n" + 
				"					\"SortAs\": \"SGML\",\n" + 
				"					\"GlossTerm\": \"Standard Generalized Markup Language\",\n" + 
				"					\"Acronym\": \"SGML\",\n" + 
				"					\"Abbrev\": \"ISO 8879:1986\",\n" + 
				"					\"GlossDef\": {\n" + 
				"                        \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\n" + 
				"						\"GlossSeeAlso\": [\"GML\", \"XML\"]\n" + 
				"                    },\n" + 
				"					\"GlossSee\": \"markup\"\n" + 
				"                }\n" + 
				"            }\n" + 
				"        }\n" + 
				"    }\n" + 
				"}";
		
		assertTrue(Json.isJson(jsonObject));
		assertTrue(Json.isJson(jsonObject2));
	}
	
	@Test
	public void testValidJson_withAJsonArray() {
		String jsonArray = "[ \"Ford\", \"BMW\", \"Fiat\" ]";
		String jsonArray2 = "[{\n" + 
				"  \"id\": 28,\n" + 
				"  \"Title\": \"Sweden\"\n" + 
				"}, {\n" + 
				"  \"id\": 56,\n" + 
				"  \"Title\": \"USA\"\n" + 
				"}, {\n" + 
				"  \"id\": 89,\n" + 
				"  \"Title\": \"England\"\n" + 
				"}]";
		assertTrue(Json.isJson(jsonArray));
		assertTrue(Json.isJson(jsonArray2));
	}

	@Test
	public void testJsonObject() {
		String jsonObject = "{\"name\":\"John\", \"age\":31, \"city\":\"New York\"}";
		String jsonObject2 = "{\n" + 
				"    \"glossary\": {\n" + 
				"        \"title\": \"example glossary\",\n" + 
				"		\"GlossDiv\": {\n" + 
				"            \"title\": \"S\",\n" + 
				"			\"GlossList\": {\n" + 
				"                \"GlossEntry\": {\n" + 
				"                    \"ID\": \"SGML\",\n" + 
				"					\"SortAs\": \"SGML\",\n" + 
				"					\"GlossTerm\": \"Standard Generalized Markup Language\",\n" + 
				"					\"Acronym\": \"SGML\",\n" + 
				"					\"Abbrev\": \"ISO 8879:1986\",\n" + 
				"					\"GlossDef\": {\n" + 
				"                        \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\n" + 
				"						\"GlossSeeAlso\": [\"GML\", \"XML\"]\n" + 
				"                    },\n" + 
				"					\"GlossSee\": \"markup\"\n" + 
				"                }\n" + 
				"            }\n" + 
				"        }\n" + 
				"    }\n" + 
				"}";
		
		assertTrue(Json.getJson(jsonObject) instanceof JsonObject);
		assertTrue(Json.getJson(jsonObject2) instanceof JsonObject);
	}

	@Test
	public void testJsonArray() {
		String jsonArray = "[ \"Ford\", \"BMW\", \"Fiat\" ]";
		String jsonArray2 = "[{\n" + 
				"  \"id\": 28,\n" + 
				"  \"Title\": \"Sweden\"\n" + 
				"}, {\n" + 
				"  \"id\": 56,\n" + 
				"  \"Title\": \"USA\"\n" + 
				"}, {\n" + 
				"  \"id\": 89,\n" + 
				"  \"Title\": \"England\"\n" + 
				"}]";
		assertTrue(Json.getJson(jsonArray) instanceof JsonArray);
		assertTrue(Json.getJson(jsonArray2) instanceof JsonArray);
	}
	
	@Test
	public void testInvalidJson() {
		String invalidJsonString = "[\n" + 
				"    \"test\" : 123\n" + 
				"]";
		String invalidJsonString2 = "{\n" + 
				"  \"Cartoon Foxes\": {\n" + 
				"    {\n" + 
				"      \"Name\": \"Fox Tall\",\n" + 
				"      \"Job\": \"Bein' tall\"\n" + 
				"    },\n" + 
				"    {\n" + 
				"      \"Name\": \"Fox Small\",\n" + 
				"      \"Job\": \"Bein' small\"\n" + 
				"    }\n" + 
				"  }\n" + 
				"}";
		assertFalse(Json.isJson(invalidJsonString));
		assertFalse(Json.isJson(invalidJsonString2));
	}	
	
	@Test
	public void testInvalidJsonException() {
		String invalidJsonString = "[\n" + 
				"    \"test\" : 123\n" + 
				"]";
		String invalidJsonString2 = "{\n" + 
				"  \"Cartoon Foxes\": {\n" + 
				"    {\n" + 
				"      \"Name\": \"Fox Tall\",\n" + 
				"      \"Job\": \"Bein' tall\"\n" + 
				"    },\n" + 
				"    {\n" + 
				"      \"Name\": \"Fox Small\",\n" + 
				"      \"Job\": \"Bein' small\"\n" + 
				"    }\n" + 
				"  }\n" + 
				"}";
		assertThrows(JsonParsingException.class, () -> Json.getJson(invalidJsonString));
		assertThrows(JsonParsingException.class, () -> Json.getJson(invalidJsonString2));
	}

	@Test
	public void testEmptyJson() {
		JsonObject object = new JsonObject();
		assertTrue(object.isEmpty());
		JsonArray array = new JsonArray();
		array.put(object);
		assertFalse(array.isEmpty());
		array.put(new JsonArray());
		assertEquals(2, array.length());
	}
}
