package com.openset.datasource.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.openset.datasource.types.Datasource;
import com.openset.datasource.types.JSON;
import com.openset.datasource.types.json.JsonArray;

@SpringBootTest
@TestPropertySource(locations = { "classpath:unit-test.properties" },
					properties = {  "datasource.type=json",
									"datasource.uri=https://openset.altervista.org/impianti_sportivi.json",
									"datasource.topics=sport"})
public class RemoteJSONTest {

	@Autowired
	private Datasource datasource;
	
	@Test
	public void whenDatasourceTypeIsJson_ThenDatasourceShouldBeInstanceOfJSON() {
		assertTrue(datasource instanceof JSON);		
	}
	
	@Test
	public void testCaseInsensitiveness() {
		JsonArray result1 = datasource.queryData("Lorenteggio", "sport");
		JsonArray result2 = datasource.queryData("LORENTEGGIO", "sport");
		JsonArray result3 = datasource.queryData("LORENTeggio", "sport");
		JsonArray result4 = datasource.queryData("lorenteggio", "sport");

		assertEquals(result1.toString(), result2.toString());
		assertEquals(result2.toString(), result3.toString());
		assertEquals(result3.toString(), result4.toString());
	}
	
	@Test
	public void testSearchSize1() {
		JsonArray result = datasource.queryData("Lorenteggio", "sport");
		assertEquals(17, result.length());
	}
	
	@Test
	public void testSearchSize2() {
		JsonArray result = datasource.queryData("pressostatico", "sport");
		assertEquals(14, result.length());
	}
}
