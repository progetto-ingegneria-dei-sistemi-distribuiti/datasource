package com.openset.datasource;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.openset.datasource.config.DatasourceConfig;
import com.openset.datasource.types.Datasource;
import com.openset.datasource.types.XML;

@SpringBootTest
@TestPropertySource(locations = { "classpath:unit-test.properties" })
public class DatasourceConfigBeansTest {
	
	@Test
	public void whenInvalidTypeInProperties_thenThrowingBCException() {
		String invalidType = "my_invalid_type";
		Exception e = assertThrows(BeanCreationException.class, 
				() -> new DatasourceConfig().getDatasource(invalidType));
		assertTrue(e.getMessage().contains(invalidType));
	}
	
	@Test
	public void whenSemiValidTypeInProperties_thenReturnDatasourceWithReflection() {
		String type = "xm";
		Datasource datasource =  new DatasourceConfig().getDatasource(type);
		assertTrue(datasource instanceof XML);
	}
	
}
