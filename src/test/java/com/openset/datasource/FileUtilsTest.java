package com.openset.datasource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileReader;
import java.io.InputStreamReader;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.openset.datasource.exceptions.FileNotFoundException;

@SpringBootTest
@TestPropertySource(locations = "classpath:unit-test.properties")
public class FileUtilsTest {

	@Test
	public void testURLHttp() {
		assertTrue(FileUtils.isWeb("http://www.example.com"));
		assertTrue(FileUtils.isWeb("https://www.example.com"));
	}
	
	@Test
	public void testLocalUri() {
		assertFalse(FileUtils.isWeb("/home/user/file.json"));
		assertFalse(FileUtils.isWeb("C:\\Utente\\file.csv"));
	}
	

	@Test
	public void testRemoteUTF8Read() {
		String expectedString = "Mathematics and Sciences:\n" + 
				"\n" + 
				"  ∮ E⋅da = Q,  n → ∞, ∑ f(i) = ∏ g(i), ∀x∈ℝ: ⌈x⌉ = −⌊−x⌋, α ∧ ¬β = ¬(¬α ∨ β),\n" + 
				"\n" + 
				"  ℕ ⊆ ℕ₀ ⊂ ℤ ⊂ ℚ ⊂ ℝ ⊂ ℂ, ⊥ < a ≠ b ≡ c ≤ d ≪ ⊤ ⇒ (A ⇔ B),\n" + 
				"\n" + 
				"  2H₂ + O₂ ⇌ 2H₂O, R = 4.7 kΩ, ⌀ 200 mm\n" + 
				"\n" + 
				"Linguistics and dictionaries:\n" + 
				"  ði ıntəˈnæʃənəl fəˈnɛtık əsoʊsiˈeıʃn\n" + 
				"  Y [ˈʏpsilɔn], Yen [jɛn], Yoga [ˈjoːgɑ]";
		assertEquals(expectedString, FileUtils.getFileFromUri("https://openset.altervista.org/utf8.txt"));
	}

	@Test
	public void testLocalUTF8Read() {
		String expectedString = "На берегу пустынных волн\n" + 
				"Стоял он, дум великих полн,\n" + 
				"И вдаль глядел. Пред ним широко\n" + 
				"Река неслася; бедный чёлн\n" + 
				"По ней стремился одиноко.\n" + 
				"По мшистым, топким берегам\n" + 
				"Чернели избы здесь и там,\n" + 
				"Приют убогого чухонца;\n" + 
				"И лес, неведомый лучам\n" + 
				"В тумане спрятанного солнца,\n" + 
				"Кругом шумел.";
		assertEquals(expectedString, FileUtils.getFileFromUri("src/test/resources/local/Bronze Horseman.txt"));
	}

	@Test
	public void testNotExistingFile() {
		assertThrows(FileNotFoundException.class, () -> FileUtils.getFileFromUri("/invalid/path/to/file.xyz"));
	}
	
	@Test
	public void testRemoteReader() {
		assertTrue(FileUtils.getReader("https://openset.altervista.org/utf8.txt") instanceof InputStreamReader);
	}
	
	@Test
	public void testLocalReader() {
		assertTrue(FileUtils.getReader("src/test/resources/local/listacomuni.csv") instanceof FileReader);
	}

	@Test
	public void testLocalReader_withNotExistingFile() {
		assertThrows(FileNotFoundException.class, () -> FileUtils.getReader("/path/to/not/existing/file"));
	}
	
	@Test
	@Disabled
	public void testRemoteReader_withNotExistingUrl() {
		assertThrows(FileNotFoundException.class, () -> FileUtils.getReader("http://www.srrzddgrfadsfsd.sss/"));
	}

	@Test
	public void testSimpleInstancing() {
		assertNotNull(new FileUtils());
	}
}
