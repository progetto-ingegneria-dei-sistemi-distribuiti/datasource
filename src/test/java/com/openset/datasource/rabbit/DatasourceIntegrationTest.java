package com.openset.datasource.rabbit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.test.RabbitListenerTestHarness;
import org.springframework.amqp.rabbit.test.RabbitListenerTestHarness.InvocationData;
import org.springframework.amqp.rabbit.test.TestRabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.test.RabbitListenerTest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import com.openset.datasource.types.Datasource;
import com.rabbitmq.client.Channel;

@SpringBootTest
@TestPropertySource(locations = { "classpath:rabbit-test.properties" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class DatasourceIntegrationTest {

	@Autowired
	@Qualifier("testTemplate")
	private TestRabbitTemplate template;

	@Autowired
	private RabbitListenerTestHarness harness;

	private Datasource datasource;
	private TestRabbitListener responseListener;

	@BeforeEach
	public void setup() {
		datasource = this.harness.getSpy("main");
		responseListener = this.harness.getSpy("responseListener");
	}

	@Test
	public void testTemplateNotNull() {
		assertNotNull(template);
		assertNotNull(harness);
	}

	@Test
	public void testSpiesNotNull() {
		assertNotNull(datasource);
		assertNotNull(responseListener);
	}

	@Test
	public void testResponse() throws Exception {
		String correlationId = UUID.randomUUID().toString();
		this.template.convertAndSend("request.comuni", "Giarre", m -> postProcessMessage(m, correlationId));

		InvocationData invocationData = this.harness.getNextInvocationDataFor("responseListener", 10, TimeUnit.SECONDS);
		assertNotNull(invocationData);
		Message actualResponse = (Message) invocationData.getArguments()[0];

		String actualResponseCorrelationId = actualResponse.getMessageProperties().getCorrelationId();
		String expectedResponseCorrelationId = correlationId;

		String actualResponseBody = new String(actualResponse.getBody());
		String expectedResponseBody = "[{\"Abitanti\":\"27785\",\"CAP\":\"95014\",\"Regione\":\"SIC\",\"Prefisso\":\"095\",\"CodFisco\":\"E017\",\"Istat\":\"087017\",\"Provincia\":\"CT\",\"Link\":\"http://www.comuni-italiani.it/087/017/\",\"Comune\":\"Giarre\"}]";

		assertEquals(expectedResponseBody, actualResponseBody);
		assertEquals(expectedResponseCorrelationId, actualResponseCorrelationId);
		verify(responseListener).catchResponse(actualResponse);
	}

	@Test
	public void testReceive() throws InterruptedException {
		String correlationId = UUID.randomUUID().toString();
		this.template.convertAndSend("request.comuni", "Patern", m -> postProcessMessage(m, correlationId));

		InvocationData invocationData = this.harness.getNextInvocationDataFor("main", 10, TimeUnit.SECONDS);
		assertNotNull(invocationData);
		Message actualResponse = (Message) invocationData.getArguments()[0];

		String actualResponseCorrelationId = actualResponse.getMessageProperties().getCorrelationId();
		String expectedResponseCorrelationId = correlationId;

		String actualResponseBody = new String(actualResponse.getBody());
		String expectedResponseBody = "Patern";

		assertEquals(expectedResponseBody, actualResponseBody);
		assertEquals(expectedResponseCorrelationId, actualResponseCorrelationId);
		verify(datasource).messageListener(actualResponse);
	}

	private Message postProcessMessage(Message m, String correlationId, String routingKey) {
		m.getMessageProperties().setCorrelationId(correlationId);
		m.getMessageProperties().setReceivedRoutingKey(routingKey);
		return m;
	}
	
	private Message postProcessMessage(Message m, String correlationId) {
		return postProcessMessage(m, correlationId, "request.comuni");
	}
	
	@TestConfiguration
	@RabbitListenerTest(capture = true)
	@EnableRabbit
	public static class RabbitConfigTest {

		@Bean
		public TestRabbitListener getListener() {
			return new TestRabbitListener();
		}

		@Bean(name = "exchange")
		@Primary
		public TopicExchange getTestResponseExchange() {
			return new TopicExchange("test", false, true);
		}

		@Bean(name = "responseQueue")
		@Primary
		public Queue getTestResponseQueue() {
			return new Queue("response.comuni", false, true, true);
		}
		
		@Bean(name = "requestQueue")
		@Primary
		public Queue getTestRequestQueue() {
			return new Queue("request.comuni", false, true, true);
		}

		@Bean
		public Binding getBinding(@Value("#{responseQueue}") Queue queue,
				@Value("#{exchange}") TopicExchange exchange) {
			return BindingBuilder.bind(queue).to(exchange).with("response.comuni");
		}

		@Bean
		public ConnectionFactory connectionFactory() {
			ConnectionFactory factory = mock(ConnectionFactory.class);
			Connection connection = mock(Connection.class);
			Channel channel = mock(Channel.class);
			willReturn(connection).given(factory).createConnection();
			willReturn(channel).given(connection).createChannel(anyBoolean());
			given(channel.isOpen()).willReturn(true);
			return factory;
		}

		@Bean(name = "testTemplate")
		public RabbitTemplate template(ConnectionFactory cf) {
			TestRabbitTemplate template = new TestRabbitTemplate(cf);
			template.setExchange("test");
			template.setUserCorrelationId(true);
			return template;
		}
		
		@Bean(name = "responseTemplate")
		@Primary
		public RabbitTemplate primaryTemplate(ConnectionFactory cf) {
			TestRabbitTemplate template = new TestRabbitTemplate(cf);
			template.setExchange("test");
			template.setUserCorrelationId(true);
			return template;
		}

		@Bean
		public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory cf) {
			SimpleRabbitListenerContainerFactory containerFactory = new SimpleRabbitListenerContainerFactory();
			containerFactory.setConnectionFactory(cf);
			return containerFactory;
		}

	}

	public static class TestRabbitListener {

		@RabbitListener(id = "responseListener", queues = "#{responseQueue.name}")
		public void catchResponse(Message msg) {
			System.out.println(new String(msg.getBody()) + msg);
		}

	}

}
