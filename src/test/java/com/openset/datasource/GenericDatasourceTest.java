package com.openset.datasource;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.openset.datasource.types.Datasource;

@SpringBootTest
@TestPropertySource(locations = { "classpath:unit-test.properties" })
public class GenericDatasourceTest {
	
	@Autowired
	private Datasource datasource;
	
	@Test
	public void test() {
		MessageProperties prop = new MessageProperties();
		prop.setCorrelationId("test correlation");
		prop.setReceivedRoutingKey("test topic");
		assertDoesNotThrow(() -> datasource.messageListener(new Message("test message".getBytes(), prop)));
	}
	
	

}
