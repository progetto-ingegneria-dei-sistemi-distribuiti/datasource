package com.openset.datasource.csv;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.openset.datasource.types.CSV;
import com.openset.datasource.types.Datasource;
import com.openset.datasource.types.json.JsonArray;

@SpringBootTest
@TestPropertySource(locations = { "classpath:unit-test.properties" },
					properties = {  "datasource.type=csv",
									"datasource.uri=https://openset.altervista.org/listacomuni.csv",
									"datasource.topics=comuni"})
public class RemoteCSVTest {

	@Autowired
	private Datasource datasource;

	@Test
	public void whenDatasourceTypeIsCsv_thenDatasourceShouldBeInstanceOfCSV() {
		assertTrue(datasource instanceof CSV);
	}
	
	@Test
	public void testCaseInsensitivenessSearch() {
		JsonArray result1 = datasource.queryData("Catania", "comuni");
		JsonArray result2 = datasource.queryData("catania", "comuni");
		JsonArray result3 = datasource.queryData("CATANIA", "comuni");
		JsonArray result4 = datasource.queryData("CaTaNiA", "comuni");

		assertEquals(result1.toString(), result2.toString());
		assertEquals(result2.toString(), result3.toString());
		assertEquals(result3.toString(), result4.toString());
	}

	@Test
	public void testSearch1() {
		JsonArray result = datasource.queryData("Catania", "comuni");
		assertTrue(!result.isEmpty());
	}

	@Test
	public void testSearch2() throws Exception {
		JsonArray result = datasource.queryData("Milano", "comuni");
		JSONAssert.assertEquals(result.toString(),
				"[{\n" + "    \"Istat\": \"015146\",\n" + "    \"Comune\": \"Milano\",\n"
						+ "    \"Provincia\": \"MI\",\n" + "    \"Regione\": \"LOM\",\n" + "    \"Prefisso\": \"02\",\n"
						+ "    \"CAP\": \"201xx\",\n" + "    \"CodFisco\": \"F205\",\n"
						+ "    \"Abitanti\": \"1324110\",\n"
						+ "    \"Link\": \"http://www.comuni-italiani.it/015/146/\"\n" + "  }]",
				JSONCompareMode.LENIENT);
	}

}

