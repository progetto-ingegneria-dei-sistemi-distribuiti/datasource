package com.openset.datasource.types;

import java.util.List;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.openset.datasource.types.json.JsonArray;

@Service
public abstract class Datasource {

	@Autowired
	@Qualifier("responseTemplate")
	private RabbitTemplate template;

	@RabbitListener(id = "main", queues = "#{requestQueue.name}")
	public void messageListener(Message receivedMessage) {
		String provenanceTopic = receivedMessage.getMessageProperties().getReceivedRoutingKey().replace("request.", "");
		String messageBody = new String(receivedMessage.getBody());
		List<String> messages = List.of(messageBody.split("\\|"));
		messages.stream().forEach(x -> searchResponse(receivedMessage,provenanceTopic,x));	
	}

	private void searchResponse(Message receivedMessage, String provenanceTopic, String messageBody) {
		List<String> messages = List.of(messageBody.split("&"));
		JsonArray queryResult = queryData(messages.get(0), provenanceTopic);
		for (String message : messages) queryResult = queryResult.searchFor(message);
		if (!queryResult.isEmpty())
			sendResponse(queryResult, provenanceTopic, receivedMessage.getMessageProperties().getCorrelationId());
	}
	
	public abstract JsonArray queryData(String message, String topic);

	private void sendResponse(JsonArray queryResult, String provenanceTopic, String correlationId) {
		String routingKey = "response" + "." + provenanceTopic;
		template.convertAndSend(routingKey, queryResult.toString().getBytes(),
				message -> postProcessMessage(message, correlationId));
	}

	private Message postProcessMessage(Message message, String correlationId) {
		message.getMessageProperties().setCorrelationId(correlationId);
		return message;
	}
}
