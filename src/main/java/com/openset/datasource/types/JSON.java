package com.openset.datasource.types;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;

import com.openset.datasource.FileUtils;
import com.openset.datasource.types.json.Json;
import com.openset.datasource.types.json.JsonArray;

public class JSON extends Datasource {

	@Value("${datasource.uri}")
	private String jsonURI;
	
	private Json json;
		
	@PostConstruct
	public void setup() {
		String jsonString = FileUtils.getFileFromUri(jsonURI);
		json = Json.getJson(jsonString);
	}
	
	@Override
	public JsonArray queryData(String message, String topic) {
		return json.searchFor(message);
	}

}
