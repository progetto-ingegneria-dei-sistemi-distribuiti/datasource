package com.openset.datasource.types;

import java.util.Collection;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;

import com.openset.datasource.types.json.JsonArray;
import com.openset.datasource.types.json.JsonObject;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterObjectFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

public class Twitter extends Datasource {

	@Value("${twitter.app_key:}")
	private String appKey;
	@Value("${twitter.app_secret:}")
	private String appSecret;

	@Value("${twitter.token:}")
	private String token;
	@Value("${twitter.token_secret:}")
	private String tokenSecret;

	private twitter4j.Twitter twitter;

	@PostConstruct
	public void setup() {
		twitterSetup(getAccessToken());
	}

	private void twitterSetup(AccessToken accessToken) {
		TwitterFactory factory = new TwitterFactory(new ConfigurationBuilder().setJSONStoreEnabled(true).build());
		twitter = factory.getInstance();
		twitter.setOAuthConsumer(appKey, appSecret);
		twitter.setOAuthAccessToken(accessToken);
	}

	private AccessToken getAccessToken() {
		AccessToken accessToken = new AccessToken(token, tokenSecret);
		return accessToken;
	}

	@Override
	public JsonArray queryData(String message, String topic) {
		JsonArray response = new JsonArray();
		for (Status tweet : getAllTweetsThatContains(message, topic)) {
			String json = TwitterObjectFactory.getRawJSON(tweet);
			response.put(new JsonObject(json));
		}
		return response;
	}

	private Collection<Status> getAllTweetsThatContains(String message, String topic) {
		Query query = new Query("#" + topic + " " + message);
		QueryResult result = search(query);
		return result.getTweets();
	}

	private QueryResult search(Query query) {
		QueryResult result = null;
		try {
			result = twitter.search(query);
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		return result;
	}

}
