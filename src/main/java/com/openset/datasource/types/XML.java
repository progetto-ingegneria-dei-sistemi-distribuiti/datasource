package com.openset.datasource.types;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;

import com.openset.datasource.FileUtils;
import com.openset.datasource.types.json.Json;
import com.openset.datasource.types.json.JsonArray;

public class XML extends Datasource {

	@Value("${datasource.uri}")
	private String xmlUri;
	
	private Json json;
	
	@PostConstruct
	public void convertXMLtoJSON() {
		String xmlFile = FileUtils.getFileFromUri(xmlUri);
		json = Json.fromXML(xmlFile);
	}

	@Override
	public JsonArray queryData(String message, String topic) {
		return json.searchFor(message);
	}

}
