package com.openset.datasource.types.json;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.openset.datasource.exceptions.JsonParsingException;

public class JsonArray implements Json {

	private JSONArray jsonArray;

	public JsonArray() {
		jsonArray = new JSONArray();
	}
	
	public JsonArray(String json) {
		try {
			jsonArray = new JSONArray(json);
		} catch (JSONException e) {
			throw new JsonParsingException(e);
		}
	}

	@Override
	public JsonArray searchFor(String message) {
		JsonArray response = new JsonArray();
		for (int i = 0; i < jsonArray.length(); i++) {
			Object object = jsonArray.get(i);
			if (object.toString().toLowerCase().contains(message.toLowerCase()))
				response.put(object);
		}
		return response;
	}

	public int length() {
		return jsonArray.length();
	}

	public boolean isEmpty() {
		return jsonArray.isEmpty();
	}

	public void put(Map<?, ?> map) {
		jsonArray.put(map);
	}

	public void put(JsonObject object) {
		jsonArray.put(object.getJSONObject());
	}

	public void put(JsonArray array) {
		jsonArray.put(array.getJSONArray());
	}
	
	private void put(Object obj) {
		jsonArray.put(obj);
	}

	@Override
	public String toString() {
		return jsonArray.toString();
	}

	protected void put(JSONObject object) {
		jsonArray.put(object);
	}

	protected JSONArray getJSONArray() {
		return jsonArray;
	}

}
