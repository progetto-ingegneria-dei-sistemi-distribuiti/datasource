package com.openset.datasource.types.json;

import org.json.JSONException;
import org.json.JSONObject;

import com.openset.datasource.exceptions.JsonParsingException;

public class JsonObject implements Json {

	private JSONObject jsonObject;

	public JsonObject() {
		jsonObject = new JSONObject();
	}

	public JsonObject(String json) {
		try {
			jsonObject = new JSONObject(json);
		} catch (JSONException e) {
			throw new JsonParsingException(e);
		}
	}
	
	protected JsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	@Override
	public JsonArray searchFor(String message) {
		JsonArray response = new JsonArray();
		for (String key : jsonObject.keySet()) {
			Object genericJsonValue = jsonObject.get(key);
			if (Json.isJson(genericJsonValue.toString()))
				response = recursiveJsonSearch(message, response, Json.getJson(genericJsonValue.toString()));
			else if (genericJsonValue.toString().toLowerCase().contains(message.toLowerCase()))
				response.put(jsonObject);
		}
		return response;
	}

	private JsonArray recursiveJsonSearch(String message, JsonArray response, Json childJson) {
		JsonArray searchResult = childJson.searchFor(message);
		if (!searchResult.isEmpty()) {
			response = putSearchResultToResponse(response, searchResult);
		}
		return response;
	}

	private JsonArray putSearchResultToResponse(JsonArray response, JsonArray searchResult) {
		if (response.isEmpty())
			response = searchResult;
		else
			response.put(searchResult);
		return response;
	}
	
	public boolean isEmpty() {
		return jsonObject.isEmpty();
	}

	protected JSONObject getJSONObject() {
		return jsonObject;
	}
	
	@Override
	public String toString() {
		return jsonObject.toString();
	}

}
