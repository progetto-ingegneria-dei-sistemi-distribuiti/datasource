package com.openset.datasource.types.json;

import org.json.XML;

import com.openset.datasource.exceptions.JsonParsingException;

public interface Json {
	public JsonArray searchFor(String message);
	
	public static boolean isJson(String json) {
		if(isJsonObject(json) || isJsonArray(json))
			return true;
		else
			return false;
	}
	
	public static Json getJson(String json) {
		if(isJsonObject(json))
			return new JsonObject(json);
		else if(isJsonArray(json))
			return new JsonArray(json);
		else
			throw new JsonParsingException("The string " + json + " is not a JSON.");
	}
	
	public static boolean isJsonObject(String json) {
		try {
			new JsonObject(json);
			return true;
		} catch (JsonParsingException e) {
			return false;
		}
	}
	
	public static boolean isJsonArray(String json) {
		try {
			new JsonArray(json);
			return true;
		} catch (JsonParsingException e) {
			return false;
		}
	}
	
	public static Json fromXML(String xml) {
		return new JsonObject(XML.toJSONObject(xml));
	}
}
