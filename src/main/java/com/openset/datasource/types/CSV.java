package com.openset.datasource.types;

import java.io.Reader;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;

import com.openset.datasource.FileUtils;
import com.openset.datasource.types.json.JsonArray;
import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

public class CSV extends Datasource {

	@Value("${datasource.uri}")
	private String csvURI;

	private List<Record> records;

	@PostConstruct
	public void parseAllRecords() {
		Reader reader = FileUtils.getReader(csvURI);
		CsvParser parser = getParser();
		records = parser.parseAllRecords(reader);
	}

	@Override
	public JsonArray queryData(String message, String topic) {
		JsonArray response = new JsonArray();
		for(Record record : records) {
			Map<String, String> recordMap = record.toFieldMap();
			for (String value : recordMap.values()) {
				if(value != null && value.toLowerCase().contains((message.toLowerCase()))) {
					response.put(recordMap);
					break;
				}
			}
		}
		return response;
	}

	private CsvParser getParser() {
		CsvParserSettings settings = new CsvParserSettings();
		settings.setHeaderExtractionEnabled(true);
		settings.setDelimiterDetectionEnabled(true);
		CsvParser parser = new CsvParser(settings);
		return parser;
	}
	
}
