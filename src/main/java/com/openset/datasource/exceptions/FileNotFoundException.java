package com.openset.datasource.exceptions;

public class FileNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -5939220155172360838L;

	public FileNotFoundException(String message) {
		super(message);
	}

}
