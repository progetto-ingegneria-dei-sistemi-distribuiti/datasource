package com.openset.datasource.exceptions;

public class JsonParsingException extends RuntimeException {

	private static final long serialVersionUID = 2057578687508197641L;

	public JsonParsingException(String message) {
		super(message);
	}
	
	public JsonParsingException(Throwable e) {
		super(e);
	}

}
