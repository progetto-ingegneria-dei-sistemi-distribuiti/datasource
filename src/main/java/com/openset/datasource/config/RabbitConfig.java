package com.openset.datasource.config;

import java.util.Collection;
import java.util.HashSet;

import javax.annotation.Resource;

import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Declarable;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

	@Value("${rabbitmq.exchange:openset}")
	private String exchangeName;

	@Resource(name = "topics")
	private String[] topics;

	@Bean(name = "requestQueue")
	public Queue getRequestAnonymousQueue() {
		return new AnonymousQueue();
	}

	@Bean(name = "exchange")
	public TopicExchange getExchange() {
		return new TopicExchange(exchangeName);
	}
	
	@Bean(name = "responseTemplate")
	public RabbitTemplate responseTemplate(ConnectionFactory cf) {
		RabbitTemplate template = new RabbitTemplate(cf);
		template.setUserCorrelationId(true);
		template.setExchange(exchangeName);
		return template;
	}

	@Bean
	public Declarables setupRequestBindings(@Value("#{exchange}") TopicExchange exchange, @Value("#{requestQueue}") Queue queue) {
		Collection<Declarable> declarables = new HashSet<>();
		for (String topic : topics)
			declarables.add(BindingBuilder.bind(queue).to(exchange).with("request" + "." + topic));
		return new Declarables(declarables);
	}

}
