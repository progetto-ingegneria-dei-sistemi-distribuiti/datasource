package com.openset.datasource.config;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.openset.datasource.types.CSV;
import com.openset.datasource.types.Datasource;
import com.openset.datasource.types.JSON;
import com.openset.datasource.types.Twitter;
import com.openset.datasource.types.XML;

@Configuration
public class DatasourceConfig {

	@Bean
	public Datasource getDatasource(@Value("${datasource.type}") String type) {
		return new DatasourceFactory().getDatasource(type);
	}

	@Bean(name = "topics")
	public String[] topics(@Value("${datasource.topics}") String topics) {
		String[] topicsArray =  topics.split(",");
		for(int i=0; i<topicsArray.length; i++)
			topicsArray[i] = topicsArray[i].strip();
		return topicsArray;
	}

	public class DatasourceFactory {

		Map<String, Supplier<Datasource>> mapOfDatasources;

		public DatasourceFactory() {
			mapOfDatasources = Map.of("twitter", Twitter::new, "csv", CSV::new, "json", JSON::new, "xml", XML::new);
		}

		public Datasource getDatasource(String type) {
			Supplier<Datasource> datasource = mapOfDatasources.get(type);
			if (datasource != null)
				return datasource.get();
			else {
				Datasource datasourceWithReflection = tryWithReflection(type);
				if (datasourceWithReflection != null) return datasourceWithReflection; 
			}
			throw new BeanCreationException("Datasource", "The specified type " + type + " is not supported");
		}

		private Datasource tryWithReflection(String type) {
			// Critic. Tested, it needs a refactor.
			Reflections reflections = new Reflections("com.openset.datasource.types",new SubTypesScanner(true));
			Set<Class<? extends Datasource>> setOfDatasource = reflections.getSubTypesOf(Datasource.class);
			String typeLowerCase = type.toLowerCase();
			Optional<Class<? extends Datasource>> datasource = setOfDatasource.stream()
					.filter(x -> x.getCanonicalName().toLowerCase().contains(typeLowerCase)).findFirst();
			
			if (datasource.isPresent()) 
				try {
					return  datasource.get().getDeclaredConstructor().newInstance();
				} catch (Exception e) {
					e.printStackTrace();
				} 
			return null; 
		}

	}

}
