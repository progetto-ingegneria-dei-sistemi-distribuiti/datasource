package com.openset.datasource.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.amqp.core.Message;

import com.openset.datasource.types.Datasource;
import com.openset.datasource.types.json.JsonArray;

public aspect LoggingAspect {

	private Logger log = LogManager.getLogger("Aspect Logger");

	pointcut logIncomingMessage(Message message) : execution(public void Datasource.messageListener(Message)) && args(message);

	before(Message message) : logIncomingMessage(message) {
		log.info("Message received: \"" + new String(message.getBody()) + "\" with correlation ID: "
				+ message.getMessageProperties().getCorrelationId());
	}

	pointcut logQueryResult(String message, String topic): execution(public JsonArray Datasource.queryData(..)) && args(message, topic) && within(Datasource+);

	after(String message, String topic) returning(JsonArray queryResult): logQueryResult(message, topic){
		if (queryResult.isEmpty())
			log.debug("Result was empty for #" + topic + " with message \"" + message + "\"");
		else
			log.debug("Search on #" + topic + " keyword " + message + " - result: " + queryResult.toString());
	}

	
}
