package com.openset.datasource.aspect;

import org.json.JSONArray;
import org.json.JSONObject;

public aspect JsonWrapperEnforcement {
	declare warning : (call(JSONArray.new(..)) || call(JSONObject.new(..))) 
						&& within(com.openset.datasource..*)
						&& !within(com.openset.datasource.types.json.*) :
	"org.json library should not be used here, use com.openset.datasource.types.Json instead";
}
