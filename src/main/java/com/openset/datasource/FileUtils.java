package com.openset.datasource;

import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.openset.datasource.exceptions.FileNotFoundException;

public class FileUtils {

	public static boolean isWeb(String uri) {
		try {
			new URL(uri);
			return true;
		} catch (MalformedURLException e) {
			return false;
		}
	}

	public static String getFileFromUri(String uri) {
		if (isWeb(uri))
			return getRemoteFile(uri);
		else
			return getLocalFile(uri);
	}

	private static String getRemoteFile(String uri) {
		try {
			InputStream inputStream = new URL(uri).openStream();
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inputStream.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
			return result.toString("UTF-8");
		} catch (IOException e) {
			throw new FileNotFoundException(e.getMessage());
		}
	}

	private static String getLocalFile(String uri) {
		try {
			return Files.readString(Paths.get(uri));
		} catch (IOException e) {
			throw new FileNotFoundException(e.getMessage());
		}
	}

	public static Reader getReader(String uri) {
		try {
			if (isWeb(uri))
				return new InputStreamReader(getInputStream(uri));
			else if (Files.exists(Paths.get(uri)))
				return new FileReader(uri);
			else
				throw new FileNotFoundException(uri);
		} catch (IOException e) {
			throw new FileNotFoundException(uri);
		}
	}

	private static InputStream getInputStream(String uri) throws IOException {
		URL url = new URL(uri);
		URLConnection connection = url.openConnection();
		connection.setConnectTimeout(5000);
		connection.setReadTimeout(5000);
		return connection.getInputStream();
	}
}
