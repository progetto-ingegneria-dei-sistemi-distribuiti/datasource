# Datasource for OpenSet

### System requirements

- **Java 11** (or higher)
- **Maven** (or use the maven wrapper provided with this repo)
- **RabbitMQ** (if you don't have an active broker or you want to test in local)

Alternatively, you can use **Docker**

## Parameters needed for run a Datasource
 1. **spring.rabbitmq.addresses** to connect to your RabbitMQ broker (default "amqp://guest:guest@localhost/").
 2. **rabbitmq.exchange** is the arbitrary name of your RabbitMQ's exchange (default "openset").
 3. **datasource.type** is the type of the file you want to share. Supported type are **twitter**, **csv** and **json**.
 4. **datasource.topics** is a [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) list of topics related to your file. Your Datasource will listen only for message of that topics.
 5. **datasource.uri**:
    - If your *datasource.type* is csv or json, enter the *datasource.uri* of your resource, either a local file or a remote resource.\
    - If *datasource.type* is twitter, you can leave this field blank and enter your *twitter developers* credential instead: **twitter.app_key**, **twitter.app_secret**, **twitter.token** and **twitter.token_secret**.


### Setting up a local istance
First of all, **Clone this repository** or download it as zip.\
If you want to build your own Datasource, you have to provide the parameters described above to the application. You have three methods to do so:
* Rename the file src/main/resources/application.properties.dist into application.properties and set the fields you need, then run `$ mvn install && mvn spring-boot:run`
* Pass the parameters as command-line argument, like `mvn install && java -jar target/Datasource*.jar --datasource.type="json" --datasource.uri="https://openset.altervista.org/impianti_sportivi.json" --datasource.topics="sport"`
* Run it through Docker `docker build -t openset:datasource . && docker run openset:datasource`. If you haven't create the application.properties, you can pass each parameter as a Docker enviroment variable like
    ``` bash
    docker run -d \
     -e spring.rabbitmq.addresses=$BROKER_URL \
     -e rabbitmq.exchange=$EXCHANGE_NAME \
     -e datasource.type=json \
     -e datasource.uri="https://openset.altervista.org/impianti_sportivi.json" \
     -e datasource.topics=sport \
    openset:datasource
    ```

### RabbitMQ setup
You can easily install RabbitMQ with Docker in two steps:
 1. `docker pull rabbitmq:management`
 2. `docker run -d -p 5672:5672 -p 15672:15672 --name my-rabbit rabbitmq:management`

Alternately, you can install RabbitMQ from your distro's package manager (the following example is for DNF-based distros like Fedora)
 - `dnf install rabbitmq-server`
 - `systemctl start rabbitmq-server.service`
